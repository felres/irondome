var back = instance_singleton( objBackgroundManager );
var camera = instance_singleton(obj_camera);
var click = mouse_check_button_pressed(mb_left);
var slide = instance_singleton(objImageSlide);


if state_new
{
    currentSpeechBubble = instance_create(x, y, objSpeechBubble);    
    currentSpeechBubble.display_text = ds_list_find_value(dialogue, 20); // the speech...
    currentSpeechBubble.image_index = 2;
    currentSpeechBubble.text_color = c_black;
    with slide scrSlideNext();
}


if click && ( currentSpeechBubble.display_text==ds_list_find_value(dialogue, 20) )    
    currentSpeechBubble.display_text = ds_list_find_value(dialogue, 21); // putty in the hands
else if click && ( currentSpeechBubble.display_text==ds_list_find_value(dialogue, 21) )   
{ 
    currentSpeechBubble.display_text = ds_list_find_value(dialogue, 22); // the generation
    with slide scrSlideNext();
    currentSpeechBubble.text_font = fntDialogueBold;
    view_shake(0, 20, 10, 0, 0);
}
else if click && ( currentSpeechBubble.display_text==ds_list_find_value(dialogue, 22) )    
    currentSpeechBubble.display_text = ds_list_find_value(dialogue, 23); // where the right to self
else if click && ( currentSpeechBubble.display_text==ds_list_find_value(dialogue, 23) )    
    currentSpeechBubble.display_text = ds_list_find_value(dialogue, 24); //with UAE
else if click && ( currentSpeechBubble.display_text==ds_list_find_value(dialogue, 24) )    
    currentSpeechBubble.display_text = ds_list_find_value(dialogue, 25); // endless plotting
else if click && ( currentSpeechBubble.display_text==ds_list_find_value(dialogue, 25) )    
    currentSpeechBubble.display_text = ds_list_find_value(dialogue, 26); // they all want
else if click && ( currentSpeechBubble.display_text==ds_list_find_value(dialogue, 26) )
if click
{
    with obj_particle instance_destroy();
    with currentSpeechBubble instance_destroy();
    state_switch(scrLevel2SceneBrain8);
}
