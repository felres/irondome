var back = instance_singleton( objBackgroundManager );
var camera = instance_singleton(obj_camera);
var click = mouse_check_button_pressed(mb_left);

if state_new
{
    obstructions_max = 200;
    var xspawn = 1612;
    var yspawn = 714;
    for(var i = 0; i < obstructions_max; i++)
    {
        if( random(2.5) < 1 )
        {
            var xx = xspawn + 35*i;
            var yy = yspawn
            if(random(3.7) < 1)
            {
                var ins = instance_create(xx, yy, objObstruction);
                ins.sprite_index = sprProtesters;
                ins.image_index = choose(0, 1);
                i+=irandom_range(2, 3);
            }
            else
            {
                var ins = instance_create(xx, yy, objObstruction);
                ins.sprite_index = sprLandRemovables;
                ins.image_index = choose(0, 1, 2, 3);
                if (ins.image_index==3) i++;
            }
        }
    }
}

with objObstruction
{
    x -= 4;
}
