screens = 0;
screens[0] = instance_find(objLevel1Screen0, 0);
screens[1] = instance_find(objLevel1Screen1, 0);
screens[2] = instance_find(objLevel1Screen2, 0);
screens[3] = instance_find(objLevel1Screen3, 0);
screens[4] = instance_find(objLevel1Screen4, 0);
screens[5] = instance_find(objLevel1Screen5, 0);
var laser = instance_find(objLaser, 0);
var back = instance_find(objBackgroundManager, 0);

if state_new
{
    omi = audio_play_sound_vol(sndOminous, 0, true, 0);
    vol = 1;
    lastvol = 1;
}
if !audio_is_playing(omi)
{
    show_debug_message("failed to play sndOminous");
    omi = audio_play_sound_vol(sndOminous, 0, true, 0);
}

if laser && back
{
    var currentScreen = collision_point(laser.x, laser.y, objLevel1ScreenParent, true, true);
    var blend = c_black;
    var len = 1;
    var chance = 1;
    var alpha = 1;
    var angspd = 0;
    var scale = random_range(20, 30);
    if(currentScreen == screens[0])
    {
        blend = c_ltgray;
        len = 1;
        chance = 4;
        alpha = 0.5;
        angspd = 0;
        scale = random_range(5, 10);
        
        // event: speech bubble #1
        if !instance_exists(currentSpeechBubble)
        {
            currentSpeechBubble = instance_create(x, y, objSpeechBubble);    
            currentSpeechBubble.display_text = ds_list_find_value(dialogue, 9);
        }
        
    }
    else if(currentScreen == screens[1])
    {
        blend = c_ltgray;
        len = 1;
        chance = 2;
        alpha = 1;
        angspd = random_range(-5, 5);
        scale = random_range(6, 10);
        
        instance_destroy(currentSpeechBubble);
    }
    else if(currentScreen == screens[2])
    {
        blend = c_gray;
        len = 1;
        chance = 1.5;
        alpha = 0.8;
        scale = random_range(10, 15);
        vol = 0.8;
        
        // event: speech bubble #2
        if !instance_exists(currentSpeechBubble)
        {
            currentSpeechBubble = instance_create(x, y, objSpeechBubble);    
            currentSpeechBubble.display_text = ds_list_find_value(dialogue, 10);
        }
    }
    else if(currentScreen == screens[3])
    {
        blend = c_dkgray;
        len = 2;
        alpha = 1;
        scale = random_range(10, 15);
        vol = 0.5;
        
        instance_destroy(currentSpeechBubble);
    }
    else if(currentScreen == screens[4])
    {
        blend = c_black;
        len = 2;
        alpha = 0.7;
        scale = random_range(25, 46);
        vol = 0.3;
        
        // event: speech bubble #3
        if !instance_exists(currentSpeechBubble)
        {
            currentSpeechBubble = instance_create(x, y, objSpeechBubble);    
            currentSpeechBubble.display_text = ds_list_find_value(dialogue, 11);
        }
    }
    else if(currentScreen == screens[5])
    {
        blend = c_black;
        len = 3;
        chance = 1;
        alpha = 1;
        angspd = random_range(-20, 20);
        scale = random_range(30, 40);
        vol = 0.1;
    }
    else
    {
        // were on a bridge most probably!. destroy speech bubble
        
        //instance_destroy(currentSpeechBubble);
    }
    
    // change sound
    if lastvol!=vol
    {
        var voltemp = 0;
        audio_sound_gain(sndIronDome, vol, voltemp);
        audio_sound_gain(omi, 1-vol, voltemp);
    }
    
    if(random(chance)<1)
    {
        for(var i = 0; i < len; i++)
        {
            var xx = random_range(view_xview[0], view_xview[0]+view_wview[0]);
            var yy = random_range(view_yview[0], view_yview[0]+view_hview[0]);
            particle_create_ext(xx, yy, sprSmoke, irandom(sprite_get_number(sprSmoke)), scale, choose(1, -1), blend, alpha,
                                0, random(360), 1, 0, angspd, blend, 0, -0.03, 5,
                                0, 0, 1);
        }
    }
}
else
{
    instance_destroy(currentSpeechBubble);
    state_switch("laserWait");
}
