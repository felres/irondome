// horizontal direction values can be (-1, 0, 1)
if state_new
{
    sprite_index = sprPlayerIdle;
    image_speed = 0;
}

if actionInputPressed
    createImpact();

if anyMovementInput
    state_switch(scrPlayerMoveDirectional);
