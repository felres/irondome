if state_new
{
    sprite_index = sprPlayerSweepBroom;
    image_speed = 8/60;
}

with instance_place(x, y, objObstruction)
{
    with instance_create(x, y, objThrowableParent)
    {
        image_speed = 0;
        image_index = other.image_index;
        sprite_index = other.sprite_index;
        
        // launch
        var len = random_range(18, 30);
        var dir = random_range(95, 130);
        hspd += lengthdir_x(len, dir);
        vspd += lengthdir_y(len, dir);
    }
    instance_destroy();
}

if( random(7) < 1 )
{
    particle_create_ext(x+89, y+178, sprSmoke, irandom(2), random_range(3, 6.5), random(360),
                        merge_colour(c_gray, c_maroon, random(0.5)), random_range(0.3, 0.5),
                        0, gauss(180, 25), random_range(1, 5), 0, gauss(0,2),
                        c_white, 0, - random_range(0.001, 0.006), depth, 0, 0, 1);
}

if actionInputPressed
{
    state_switch(scrPlayerBroomLift);
}
