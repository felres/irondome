var panel1 = instance_find_team(objAnythingMask, "intro");
var panel2 = instance_find_team(objAnythingMask, "intro2");
var click = mouse_check_button_pressed(mb_left);
var focusChar = objLevel1FlyingDownChar;
var introbuilding = objLevel1Background;
if state_new
{
    currentSpeechBubble = instance_create(x, y, objSpeechBubble);    
    currentSpeechBubble.display_text = ds_list_find_value(dialogue, 0);
    focusChar.sprite_index = sprPlayerSpeechB;
    focusChar.image_speed = 4/60;
    // passive flashin
    with introbuilding
        alarm[2] = random(60);
}


if (state_timer>=dialogue_cooldown) && click
{
    instance_destroy(currentSpeechBubble);
    state_switch("speechBubbleIntro2");
}
