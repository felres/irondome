var back = instance_singleton( objBackgroundManager );
var camera = instance_singleton(obj_camera);
var click = mouse_check_button_pressed(mb_left);
var slide = instance_singleton(objImageSlide);


if state_new
{
    camera.x = slide.x + view_wview[0]/2 + 120;
    camera.y = slide.y + view_hview[0]/2;
}

/// pan camera to tinfoil
with camera
{
    view_position_update_lerp(0, 0.085, id);
}

if (state_timer>=1*room_speed) && (slide.image_index==0)
    with slide scrSlideNext();
if (state_timer>=1.3*room_speed) && (slide.image_index==1)
    with slide scrSlideNext();

if click && (slide.image_index == 2)
{
    view_position_update_jump(0, camera);
    with obj_particle instance_destroy();
    with currentSpeechBubble instance_destroy();
    state_switch(scrLevel2SceneBrain4);
}
