var back = instance_singleton( objBackgroundManager );
var camera = instance_singleton(obj_camera);
var click = mouse_check_button_pressed(mb_left);

if state_new
{
    with objLevel1CloseUp instance_destroy();
    with camera 
    {
        state = 1;
        follow = objPlayer;
        follow_xoffset = +80;
        follow_yoffset = -66;
    }
}

state_switch(scrLevel2GamePlaying)
