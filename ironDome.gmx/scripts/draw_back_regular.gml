var x1, y1, x2, y2, off, col1, col2, col3, col4, wav;
off = 16;
x1 = view_xview[0] - off;
y1 = view_yview[0] - off;
x2 = view_xview[0] + view_wview[0] + off;
y2 = view_yview[0] + view_hview[0] + off;

i += ispeed;
wav = sin(i)*0.5+0.5;
col1 = merge_colour(def_col1, def_col2, wav);
col2 = col1;
col3 = merge_colour(def_col1, def_col2, 1 - wav);
col4 = col3;
draw_rectangle_colour(x1, y1, x2, y2, col1, col2, col3, col4, false);
