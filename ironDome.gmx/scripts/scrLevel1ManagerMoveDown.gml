var panel1 = instance_find_team(objAnythingMask, "intro");
var panel2 = instance_find_team(objAnythingMask, "intro2");
var click = mouse_check_button_pressed(mb_left);
if state_new instance_create(obj_camera.x, obj_camera.y - 270, objLevel1FlyingDownChar);
var focusChar = objLevel1FlyingDownChar;
var char_landing = objPlayerIntroLandingSpot;
var introbuilding = objLevel1Background;
var charYoffset = -150;

var spd = 8;

// manage char movement
with focusChar
{
    var yy = char_landing.y; // y objective
    y = approach(y, yy, spd);
    if click spd *= 2; // hurry up
    if (state==0)&&(y == yy)
    {
        // Landed
        state = 1;
        // right smoke
        particle_create_ext(x, y+sprite_height*0.5, sprSmoke, 0, 3.5, random(360), c_white, 0.9, 0.2, 20,     3, 0, 3, c_white, 0, -0.01, -100, 1, 0, 0);
        particle_create_ext(x, y+sprite_height*0.5, sprSmoke, 0, 2.0, random(360),c_ltgray, 0.9, 0.2, 30,     4, 0, 3, c_white, 0, -0.01, -101, 1, 0, 0);
        particle_create_ext(x, y+sprite_height*0.5, sprSmoke, 0, 2.0, random(360),c_ltgray, 0.9, 0.2,  0,     4, 0, 3, c_white, 0, -0.01, -101, 1, 0, 0);
        particle_create_ext(x, y+sprite_height*0.5, sprSmoke, 0, 2.0, random(360),c_ltgray, 0.9, 0.2, 15,     4, 0, 3, c_white, 0, -0.01, -101, 1, 0, 0);
        // left smoke
        particle_create_ext(x, y+sprite_height*0.5, sprSmoke, 0, 3.5, random(360), c_white, 0.9, 0.2, 180-20, 3, 0, 3, c_white, 0, -0.01, -100, 1, 0, 0);
        particle_create_ext(x, y+sprite_height*0.5, sprSmoke, 0, 2.0, random(360),c_ltgray, 0.9, 0.2, 180-10, 4, 0, 3, c_white, 0, -0.01, -101, 1, 0, 0);
        particle_create_ext(x, y+sprite_height*0.5, sprSmoke, 0, 2.0, random(360),c_ltgray, 0.9, 0.2, 180-00, 4, 0, 3, c_white, 0, -0.01, -101, 1, 0, 0);
        particle_create_ext(x, y+sprite_height*0.5, sprSmoke, 0, 2.0, random(360),c_ltgray, 0.9, 0.2, 180-15, 4, 0, 3, c_white, 0, -0.01, -101, 1, 0, 0);
        view_shake(0,10,20,0,0);
        audio_play_sound_vol(sndLanding, 1, false, 0.5);
        with introbuilding
            alarm[0] = animwait;
    }
}

// camera follow char
if (focusChar.state == 0)
{
    panel1.y = max(panel1.y, focusChar.y + charYoffset);
    panel1.y = min(panel1.y, panel2.y);
}
with instance_singleton(obj_camera)
{
    focus_on_panel(panel1, other.panelfocusoffset, 0.2);
}

// next state
if click && (focusChar.state == 1)
    state_switch("speechBubbleIntro");
