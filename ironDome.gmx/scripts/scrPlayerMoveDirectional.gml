// horizontal direction values can be (-1, 0, 1)
if state_new
{
    sprite_index = sprPlayerFlying;
    image_speed = 0;
    image_index = 0; //hp
}
// read input
horDir = -leftInput+rightInput;
verDir = -upInput+downInput;

// set speed
if( (abs(horDir) == 0) && (abs(verDir) == 0) )
    spd = approach(spd, 0, decel);
else
{
    spd = max(approach(spd, spdMax, acc), spdInit);
    angleDir = angle_approach(angleDir, point_direction(x, y, x+horDir, y+verDir), turnspeed);
}
// set individual speeds
horSpd = lengthdir_x(spd, angleDir);
verSpd = lengthdir_y(spd, angleDir);

/// prevent clipping
var perdon = 4 * spd/spdMax; // pixeles proporcional a spd
if( (sign(horSpd) != 0)&&(place_meeting(x+horSpd, y, collision_object)) )
{
    // primero probar perdon por casi pasarse
    if !place_meeting(x+horSpd, y+perdon, collision_object)
        y += perdon;
    else if !place_meeting(x+horSpd, y-perdon, collision_object)
        y -= perdon;
    // no se pudo el perdon, detener
    else
    {
        while ( !place_meeting(x+sign(horSpd), y, collision_object) )
        {
            x+=sign(horSpd);
        }
        spd = 0;
        horSpd = 0;
    }
    
}
if( (sign(verSpd) != 0)&&(place_meeting(x, y+verSpd, collision_object)) )
{
    // primero probar perdon por casi pasarse
    if !place_meeting(x+perdon, y+verSpd, collision_object)
        x += perdon;
    else if !place_meeting(x-perdon, y+verSpd, collision_object)
        x -= perdon;
    // no se pudo el perdon, detener
    else
    {
        while ( !place_meeting(x, y+sign(verSpd), collision_object) )
        {
            y+=sign(verSpd);
        }
        spd = 0;
        verSpd = 0;
    }
}

// change final coordinates
x += horSpd;
y += verSpd;


if !state_new && (spd == 0)
    state_switch(scrPlayerIdleDirectional);
