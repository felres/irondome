///tmc_string_is_empty(str)
/*
    returns if the string is a empty of characters ("")
    
    WARNING the functions allow non alphabetical characters
    
    var t = tmc_string_is_empty(""); //t = 1;
    var t = tmc_string_is_empty("abc"); //t = 0;
    
    
*/

return(argument0 == "");
