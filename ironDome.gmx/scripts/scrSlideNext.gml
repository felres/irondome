var spd = 0.4;
switch image_index
{
    case 6:
        scrSlideSpecialTransition(sprTransition_1, spd);
        break;
    case 7:
        scrSlideSpecialTransition(sprTransition_2, spd);
        break;
    case 10:
        scrSlideSpecialTransition(sprTransition_3, spd);
        break;
    case 11:
        scrSlideSpecialTransition(sprTransition_4, spd);
        break;
}
image_index++;

/// talker
switch(image_index)
{
    case 3:
    case 6:
    case 9:
        myTalker.sprite_index = sprBrainSceneTinFoilSpeak1;
        break;
    case 5:
        myTalker.sprite_index = sprBrainSceneTinFoilSpeak2;
        break;
    case 4:
        myTalker.sprite_index = sprBrainSceneIronDomeSpeak1;
        break;
    case 8:
    case 10:
        myTalker.sprite_index = sprBrainSceneIronDomeSpeak2;
        break;
    default:
        myTalker.sprite_index = _sprEmpty;
        myTalker.depth = depth-1;
        myTalker.x = x;
        myTalker.y = y;
        break;
}
