if state_new 
    with instance_create(x, y, objBlackFadeOut)
        ready = false;
        

var click = mouse_check_button_pressed(mb_left);
var panel1 = instance_find_team(objAnythingMask, "intro");
var logo = instance_find(objLevel1Logo, 0);

with instance_singleton(obj_camera)
{
    state = 0;
    focus_on_panel(panel1, other.panelfocusoffset, 1);
}
with logo
    image_speed = 0;

if click || (state_timer>=room_speed*0.5)
    state_switch("title");
