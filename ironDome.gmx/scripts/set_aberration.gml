///set_aberration
var steps = argument0;
var amount = argument1;

with( objDefaultDrawManager )
{
    dis = amount;
    dis_spd = amount/(steps * 1.5);
}
