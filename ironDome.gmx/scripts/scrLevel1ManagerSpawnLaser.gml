if state_new
{
    audio_stop_all();
    // respawn laser
    with objLaserSpawnPoint event_user(0);
    // make camera follow
    with instance_singleton(obj_camera) state = 2;
    // restart background
    with objBackgroundManager state_switch("white");
    state_switch("running");
}
