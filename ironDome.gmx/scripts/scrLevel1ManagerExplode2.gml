var laser = instance_find(objLaser, 0);
var back = instance_find(objBackgroundManager, 0);
var panelFinal = instance_find(objLevel1Screen5, 0);

// zoomin
var camoffset = -0.1;
with instance_singleton(obj_camera)
{
    state = 0;
    focus_on_panel(panelFinal, camoffset, 0.008);
}

if !audio_is_playing(timbal)
{
    state_switch("explode3");
}
