var camera = instance_singleton(obj_camera);
var str1 = ds_list_find_value(text_extras, 3);

if state_new
{
    audio_play_sound_vol(sndOminous, 0, true, 0.2);
    instance_create(x, y, objFadeInto);
}

if objFadeInto.image_alpha >= 0.9 && !instance_exists(objTextDrawer)
{
    audio_play_sound(sndLaugh, 5, false);
    title = instance_create(x, y, objTextDrawer);
    draw_set_font(fntTitle);
    title.x = view_xview[0] + view_wview[0]/2;
    title.y = view_yview[0] + view_hview[0]/2;
    title.txt = str1;
    title.fnt = fntTitle;
    title.fa_hor = fa_center;
    title.fa_ver = fa_middle;
}
