var panel1 = instance_find_team(objAnythingMask, "intro");
var panel2 = instance_find_team(objAnythingMask, "intro2");
var panelplayerspawn = instance_find_team(objAnythingMask, "levelstart");
var click = mouse_check_button_pressed(mb_left);
var focusChar = objLevel1FlyingDownChar;
var introbuilding = objLevel1Background;

if state_new
{    
    xx = view_xview[0] - 10;
    yy = view_yview[0];
    fullbody = instance_create(xx, yy, objLevel1CloseUp);
    with fullbody
    {
        sprite_index = sprLevel1Fullbody;
        image_speed = 4/60;
        y-= sprite_height - 480
    }
}

fullbody.y = approach(fullbody.y, fullbody.ystart, 6);
if (fullbody.y==fullbody.ystart) && !instance_exists(objSpeechBubble)
{
    currentSpeechBubble = instance_create(x, y, objSpeechBubble);
    currentSpeechBubble.display_text = ds_list_find_value(dialogue, 8);
    audio_play_sound_vol(sndCheer, 5, false, 0.9);
}

if (state_timer>=dialogue_cooldown) && click
{
    instance_destroy(currentSpeechBubble);
    state_switch("fullBodyPrepareLaser");
}
