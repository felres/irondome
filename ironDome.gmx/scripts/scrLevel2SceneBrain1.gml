var back = instance_singleton( objBackgroundManager );
var camera = instance_singleton(obj_camera);
var click = mouse_check_button_pressed(mb_left);
var slide = instance_singleton(objImageSlide);
if state_new
{
    view_shake(0, 25, 20, 0, 5);
    with back state_switch("black");
    var xx = view_xview[0];
    var yy = view_yview[0];
    slide.x = xx;
    slide.y = yy;
    var fadeback = instance_create(xx, yy, objBlackFadeOut);
    fadeback.fadeSpd = 0.02;
    with camera
    {
        x = slide.x + view_wview[0]/2;
        y = slide.y + view_hview[0]/2;
        view_position_update_jump(0, id);
    }
}

var ytarget = slide.y + view_hview[0]/2 + (slide.sprite_height - view_hview[0]);
/// lower camera
with camera
{
    y = approach(y, ytarget, 5);
    view_position_update_lerp(0, 0.6, id);
}

if click && !instance_exists(currentSpeechBubble)
{
    with camera
    {
        y = ytarget;
        view_position_update_jump(0, id);
    }
    currentSpeechBubble = instance_create(x, y, objSpeechBubble);    
    currentSpeechBubble.display_text = ds_list_find_value(dialogue, 14); // it took you
    currentSpeechBubble.image_index = 2;
    currentSpeechBubble.text_color = c_black;
    currentSpeechBubble.text_font = fntDialogueBold;
}



if click && instance_exists(currentSpeechBubble) && (currentSpeechBubble.timer>=30)
{
    with obj_particle instance_destroy();
    with currentSpeechBubble instance_destroy();
    state_switch(scrLevel2SceneBrain2);
}
