var click = mouse_check_button_pressed(mb_left);

if state_new
{
    xx = view_xview[0] + view_wview[0]/2;
    yy = view_yview[0] + view_hview[0]/2;
    closeup = instance_create(xx, yy, objLevel1CloseUp);
    with closeup
    {
        sprite_index = sprBackFrame;
        set_scale(1.1);
        image_alpha = 0;
        depth = -110
    }
}

closeup.image_alpha = approach(closeup.image_alpha, 1, 0.003);
if (closeup.image_alpha == 1) && !instance_exists(objSpeechBubble)
{
    currentSpeechBubble = instance_create(x, y, objSpeechBubble);
    currentSpeechBubble.display_text = ds_list_find_value(dialogue, 12);
}

if instance_exists(objSpeechBubble) && (state_timer>=30) && click
{
    //instance_destroy(currentSpeechBubble);
    state_switch("explode5");
}
