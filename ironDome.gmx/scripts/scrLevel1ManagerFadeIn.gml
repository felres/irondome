

var click = mouse_check_button_pressed(mb_left);
var panel1 = instance_find_team(objAnythingMask, "intro");
var fadeback = instance_find(objBlackFadeOut, 0);
var camera = instance_singleton(obj_camera);
var logo = instance_find(objLevel1Logo, 0);

if state_new
{
    intro_snd = audio_play_sound_vol(sndHeroicLoop, 100, true, 0.5);
    with logo image_speed = 10/60;
    if instance_exists(fadeback)
        with fadeback{ready = true; fadeSpd = 0.015;}
}

if click
    state_switch("moveDown");
