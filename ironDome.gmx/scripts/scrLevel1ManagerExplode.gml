var laser = instance_find(objLaser, 0);
var back = instance_find(objBackgroundManager, 0);
var panelFinal = instance_find(objLevel1Screen5, 0);
if state_new
{
    instance_destroy(laser);
    instance_destroy(objCursor);
    instance_destroy(objCompleteTrigger);
    instance_destroy(objSpeechBubble);
    audio_stop_all();
    timbal = audio_play_sound_vol(sndTimbal, 0, false, 1);
    with back state_switch("violence");
    with objConector
        visible = false;
}

if !audio_is_playing(timbal)
{
    show_debug_message("failed to play timbal");
    timbal = audio_play_sound(sndTimbal, 0, false);
}

if audio_is_playing(timbal)
{
    state_switch("explode2");
}
