var back = instance_singleton( objBackgroundManager );
var camera = instance_singleton(obj_camera);
var click = mouse_check_button_pressed(mb_left);
var slide = instance_singleton(objImageSlide);


if state_new
{
    currentSpeechBubble = instance_create(x, y, objSpeechBubble);    
    currentSpeechBubble.display_text = ds_list_find_value(dialogue, 17); // so glad
    currentSpeechBubble.image_index = 2;
    currentSpeechBubble.text_color = c_black;
    with camera
    {
        var ss = 0.62;
        view_size_update_jump(0, ss*default_view_w, ss*default_view_h);
        x = slide.x + view_wview[0]/2;
        y = slide.y + view_hview[0]/2;
        view_position_update_jump(0, id);
    }
    with slide scrSlideNext();
}

if click && ( currentSpeechBubble.display_text==ds_list_find_value(dialogue, 17) )
{
    currentSpeechBubble.display_text = ds_list_find_value(dialogue, 18); // its a PR nightmare
    currentSpeechBubble.text_font = fntDialogueBold;
}
else if click && ( currentSpeechBubble.display_text==ds_list_find_value(dialogue, 18) )
{
    with obj_particle instance_destroy();
    with currentSpeechBubble instance_destroy();
    state_switch(scrLevel2SceneBrain6);
}
