if state_new
{
    with objBackgroundManager 
        state_switch("white");
}

var click = mouse_check_button_pressed(mb_left);
with instance_singleton(obj_camera)
{
    focus_on_panel(objLevel1Screen0, other.panelfocusoffset, 0.2);
}

if click && (state_timer > 1*room_speed)
{
    state_switch("spawnLaser");
}
