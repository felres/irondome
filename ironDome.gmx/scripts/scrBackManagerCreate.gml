col1 = c_white;
col2 = col1;
col3 = col1;
col4 = col1;
i = 0;
wav = 0;
changespd = 0.1;
custom_color = c_white;

/// FSM
state_machine_init();

state_create("celeste", scrBackManagerCeleste);
state_create("white", scrBackManagerWhite);
state_create("black", scrBackManagerBlack);
state_create("violence", scrBackManagerViolence);
state_create("sparks", scrBackManagerSparks);
state_create("custom", scrBackManagerCustom);

state_init("celeste");
