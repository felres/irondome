// horizontal direction values can be (-1, 0, 1)
horDir = -leftInput+rightInput;
verDir = -upInput+downInput;
if( (abs(horDir) == 0) && (abs(verDir) == 0) )
    spd = spdInit;
else
{
    spd = approach(spd, spdMax, acc);
    // Angle at which player is pointing
    angleDir = point_direction(x, y, x+horDir, y+verDir);
}
horSpd = lengthdir_x(spd, angleDir) * abs(horDir);
verSpd = lengthdir_y(spd, angleDir) * abs(verDir);
// change final coordinates
x += horSpd + horExt;
y += verSpd + verExt;
