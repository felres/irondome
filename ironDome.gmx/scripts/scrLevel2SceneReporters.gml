var back = instance_singleton( objBackgroundManager );
var camera = instance_singleton(obj_camera);
var click = mouse_check_button_pressed(mb_left);
var crapface = instance_singleton(objLevel1CloseUp);
if state_new
{
    var s = 1
    with camera view_size_update_jump(0, default_view_w*s, default_view_h*s);
    with crapface
    {
        sprite_index = sprCrapFace;
        image_speed = 0;
        x = view_xview[0];
        y = view_yview[0]+view_hview[0];
    }
    with back
    {
        state_switch("custom");
        custom_color = make_colour_rgb(214,202,113)
    }
}

crapface.y = approach(crapface.y, view_yview[0], 10);

if (crapface.y == view_yview[0])
    state_switch(scrLevel2SceneReporters2);
