audio_stop_all();
panelfocusoffset = 0.1; // 0.05
text_extras = get_dialogue_batches(working_directory + "extras_" + global.language + ".txt");
dialogue = get_dialogue_batches(working_directory + "dialogue_" + global.language + ".txt");
dialogue_cooldown = 0;

/// FSM
state_machine_init();

state_create("start", scrLevel1ManagerStart);
state_create("title", scrLevel1ManagerTitle);
state_create("fadeIn", scrLevel1ManagerFadeIn);
state_create("moveDown", scrLevel1ManagerMoveDown);
state_create("speechBubbleIntro", scrLevel1ManagerSpeechBubbleIntro);
state_create("speechBubbleIntro2", scrLevel1ManagerSpeechBubbleIntro2);
state_create("speechBubbleIntro3", scrLevel1ManagerSpeechBubbleIntro3);
state_create("speechBubbleIntro4", scrLevel1ManagerSpeechBubbleIntro4);
state_create("speechBubbleIntro5", scrLevel1ManagerSpeechBubbleIntro5);
state_create("speechBubbleIntro6", scrLevel1ManagerSpeechBubbleIntro6);
state_create("speechBubbleIntro7", scrLevel1ManagerSpeechBubbleIntro7);
state_create("speechBubbleIntro8", scrLevel1ManagerSpeechBubbleIntro8);
state_create("fullBody", scrLevel1ManagerFullBody);
state_create("fullBodyPrepareLaser", scrLevel1ManagerFullBodyPrepareLaser);
state_create("shootLaserAnim", scrLevel1ManagerShootLaserAnim);
state_create("waitingLaserAnimToEnd", scrLevel1ManagerWaitingLaserAnimToEnd);
state_create("panCameraLaserSpawn", scrLevel1ManagerPanCameraLaserSpawn);
state_create("spawnLaser", scrLevel1ManagerSpawnLaser);
state_create("running", scrLevel1ManagerRunning);
state_create("laserWait", scrLevel1ManagerLaserWait);
state_create("explode", scrLevel1ManagerExplode);
state_create("explode2", scrLevel1ManagerExplode2);
state_create("explode3", scrLevel1ManagerExplode3);
state_create("explode4", scrLevel1ManagerExplode4);
state_create("explode5", scrLevel1ManagerExplode5);
//state_create("", scrLevel1Manager);

state_init("start");
