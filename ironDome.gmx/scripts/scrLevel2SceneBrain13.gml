var back = instance_singleton( objBackgroundManager );
var camera = instance_singleton(obj_camera);
var click = mouse_check_button_pressed(mb_left);


if state_new
{
    currentSpeechBubble = instance_create(x, y, objSpeechBubble);    
    currentSpeechBubble.display_text = ds_list_find_value(dialogue, 32); // hm think about it
    currentSpeechBubble.image_index = 2;
    currentSpeechBubble.text_color = c_black;
    currentSpeechBubble.text_font = fntDialogueBold;
}

if click && instance_exists(objImageSlide) && (objImageSlide.image_index==11)
{
    // to dark
    with objImageSlide scrSlideNext();
}
else if click && instance_exists(objImageSlide) && (objImageSlide.image_index==12)
{
    
    with objImageSlide instance_destroy();
    with obj_particle instance_destroy();
    with currentSpeechBubble instance_destroy();
    state_switch(scrLevel2SceneReporters);
}
