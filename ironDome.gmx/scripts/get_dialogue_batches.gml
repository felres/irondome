var fname = argument0;
var str = file_text_open_read_all(fname);

var list = string_split(str, "##");
for(var i = 0; i < ds_list_size(list); i++)
{
    // trim first "#" char
    var par = ds_list_find_value(list, i);
    var newpar = tmc_string_truncate_left(par, string_length(par)-1);
    ds_list_replace(list, i, newpar);
    // debug
    show_debug_message("Element NO."+string(i)+": ["+newpar+"]");
}
return list;
