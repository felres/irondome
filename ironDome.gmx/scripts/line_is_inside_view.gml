/// line_is_inside_view(x1, y1, x2, y2, view)

var x1, y1, x2, y2, x3, y3, x4, y4, view;

v = argument4;
x1 = argument0;
y1 = argument1;
x2 = argument2;
y2 = argument3;
x3 = view_xview[v];
y3 = view_yview[v];
x4 = x3 + view_wview[v];
y4 = y3 + view_hview[v];

return point_in_rectangle(x1, y1, x3, y3, x4, y4) || point_in_rectangle(x2, y2, x3, y3, x4, y4)
