var panel1 = instance_find_team(objAnythingMask, "intro");
var panel2 = instance_find_team(objAnythingMask, "intro2");
var panelplayerspawn = instance_find_team(objAnythingMask, "levelstart");
var click = mouse_check_button_pressed(mb_left);
var focusChar = objLevel1FlyingDownChar;
var introbuilding = objLevel1Background;

if state_new
{
    with objLevel1CloseUp
        instance_destroy();
    xx = view_xview[0] - 10;
    yy = view_yview[0];
    fullbody = instance_create(xx, yy, objLevel1CloseUp);
    fullbody.sprite_index = sprLevel1Laser;
    if audio_is_playing(sndCheer)
        audio_stop_sound(sndCheer);
}

fullbody.image_blend = merge_colour(c_red, c_white, random(0.5));

var max_timer_duration = 30;
if (state_timer>=max_timer_duration)
{
    instance_destroy(fullbody);
    state_switch("shootLaserAnim");
}
