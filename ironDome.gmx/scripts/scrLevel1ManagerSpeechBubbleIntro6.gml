var panel1 = instance_find_team(objAnythingMask, "intro");
var panel2 = instance_find_team(objAnythingMask, "intro2");
var click = mouse_check_button_pressed(mb_left);
var focusChar = objLevel1FlyingDownChar;
var introbuilding = objLevel1Background;

if state_new
{
    show_debug_message("in state SpeechBubbleIntro6");
    currentSpeechBubble = instance_create(x, y, objSpeechBubble);
    currentSpeechBubble.display_text = ds_list_find_value(dialogue, 5);
    focusChar.sprite_index = sprPlayerSpeechB;
}

if (state_timer>=dialogue_cooldown) && click
{
    instance_destroy(currentSpeechBubble);
    state_switch("speechBubbleIntro7");
}
