/// draw_sprite_segmented(sprite, columns)
var _spr = argument0;
var cols = argument1;
var w = sprite_get_width(_spr);
var h = sprite_get_height(_spr);

for(var i = 0; i < sprite_get_number(_spr); i++)
{
    var xx = w * (i mod cols)
    var yy = h * (i div cols)
    draw_sprite(_spr, i, xx, yy);
}

