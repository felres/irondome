/// particle_create_ext(x, y, sprite_index, image_index, image_scale, image_angle, image_blend, image_alpha, image_speed, move_direction, move_speed, scale_speed, angle_speed, color_target, blend_speed, alpha_speed, depth, destroy_at_anim_end?, destroy_at_alpha_more_than_1?, destroy_at_alpha_less_than_0?)
var p = instance_create(argument[0], argument[1], obj_particle);
with p
{
    
    sprite_index = argument[2];
    image_index = argument[3];
    set_scale(argument[4]);
    image_angle = argument[5];
    image_blend = argument[6];
    image_alpha = argument[7];
    image_speed = argument[8];
    move_direction = argument[9];
    move_speed = argument[10];
    scale_speed = argument[11];
    angle_speed = argument[12];
    color_target = argument[13];
    blend_speed = argument[14];
    alpha_speed = argument[15];
    depth = argument[16];
    destroy_at_anim_end = argument[17];
    destroy_at_alpha_more_than_1 = argument[18];
    destroy_at_alpha_less_than_0 = argument[19];
}

return p;
