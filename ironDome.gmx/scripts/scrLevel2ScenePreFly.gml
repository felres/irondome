var back = instance_singleton( objBackgroundManager );
var camera = instance_singleton(obj_camera);
var click = mouse_check_button_pressed(mb_left);
var d = -1;
if state_new
{
    with camera
    {
        state = 0;
        view_position_update_jump(0, room_width/2, -500);
    }
    with back
    {
        state_switch("custom");
        custom_color = make_colour_rgb(240,231,132)
        //depth = d+1;
    }
    
    var xx = view_xview[0] + view_wview[0]/2;
    var yy = view_yview[0] + view_hview[0]/2;
    // doesnt work in HTML for some annoying reason
    //particle_create_ext(xx, yy, sprSkyCenter, 0, 3, 0, c_white, 1, 0, 0, 0, 0, 0, c_white, 0, 0, -2, 0, 0, 0);
    //particle_create_ext(xx, yy, sprIDFLying, 0, 1, 0, c_white, 1, 6/60, 0, 0, 0, 0, c_white, 0, 0, d-2, 0, 0, 0);
    with instance_create(xx, yy, objLevel1CloseUp)
    {
        sprite_index = sprIDFLying;
        image_speed = 6/60;
        depth = -3;
    }
    with instance_create(xx, yy, objLevel1CloseUp)
    {
        sprite_index = sprSkyCenter;
        image_speed = 0;
        depth = -1;
        set_scale(3);
    }
}



if ( (state_timer mod 55) == 0 )
{
    var xx = view_xview[0] + view_wview[0]/2;
    var yy = view_yview[0] + view_hview[0]/2;
    smokes = 3;
    var startang = 90 + (state_timer div 50)*33;
    var movespd = 9;
    var scalespd = 0.025;
    var anglespd = -0.4;
    var alphaspd = -0.004;
    for(var i = 0; i < smokes; i++)
    {
        var s = gauss(0.8, 0.04);
        var angle = startang + (360/smokes)*i;
        particle_create_ext(xx, yy, sprFlyCloud, 0, s, angle, c_white, 1, 0, angle, movespd, scalespd, anglespd, c_white, 0, alphaspd, d-1, 0, 0, 1);
    }
}

if instance_exists(currentSpeechBubble)
{
    with camera
    {
        var ss = 0.61;
        view_size_update_lerp(0, 0.1, ss*default_view_w, ss*default_view_h);
    }
}

if click
{
    if !instance_exists(currentSpeechBubble)
    {
        currentSpeechBubble = instance_create(x, y, objSpeechBubble);    
        currentSpeechBubble.display_text = ds_list_find_value(dialogue, 13);
    }
    else
    {
        with objLevel1CloseUp instance_destroy();
        with obj_particle instance_destroy();
        with currentSpeechBubble instance_destroy();
        state_switch(scrLevel2SceneBrain1);
    }
}
