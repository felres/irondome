

var click = mouse_check_button_pressed(mb_left);
var panel1 = instance_find_team(objAnythingMask, "intro");
var fadeback = instance_find(objBlackFadeOut, 0);
var title = instance_singleton(objTextDrawer);
var camera = instance_singleton(obj_camera);
var str1 = ds_list_find_value(text_extras, 0);
var str2 = str1 + "#" + ds_list_find_value(text_extras, 1);

if state_new
{
    draw_set_font(fntTitle);
    title.x = camera.x;
    title.y = camera.y - string_width(str2);
    title.txt = str1;
    title.fnt = fntTitle;
    audio_play_sound(sndDramaticDun, 1, false);
}
else if (title.txt == str1)&&((state_timer>=room_speed*1)||click)
{
    title.txt = str2;
    audio_play_sound(sndDramaticDun, 1, false);
}


if (click&&(title.txt = str2)) || (state_timer>=room_speed*2)
{
    instance_destroy(title);
    state_switch("fadeIn");
}
