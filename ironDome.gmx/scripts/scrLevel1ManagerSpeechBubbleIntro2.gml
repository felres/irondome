var panel1 = instance_find_team(objAnythingMask, "intro");
var panel2 = instance_find_team(objAnythingMask, "intro2");
var click = mouse_check_button_pressed(mb_left);
var focusChar = objLevel1FlyingDownChar;
var introbuilding = objLevel1Background;


if state_new
{    
    xx = view_xview[0] - 10;
    yy = view_yview[0];
    closeup = instance_create(xx, yy, objLevel1CloseUp);
    closeup.image_speed = 0;
    closeup.image_alpha = 0;
}

closeup.image_alpha = approach(closeup.image_alpha, 1, 0.05);
if (closeup.image_alpha==1) && !instance_exists(objSpeechBubble)
{
    currentSpeechBubble = instance_create(x, y, objSpeechBubble);
    currentSpeechBubble.display_text = ds_list_find_value(dialogue, 1);
    closeup.image_speed = 4/60;
}

if (state_timer>=dialogue_cooldown) && click
{
    instance_destroy(currentSpeechBubble);
    state_switch("speechBubbleIntro3");
}
