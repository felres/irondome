var back = instance_find(objBackgroundManager, 0);
var panelFinal = instance_find(objLevel1Screen5, 0);
if state_new
{
    with back state_switch("sparks");
    view_shake(0,120,60,4,1);
    audio_play_sound(sndExplosionFull, 1, false);
    panelFinal.sprite_index = sprCitymiddleground;
    panelFinal.y += sprite_get_height(sprLevel1Screen5) - sprite_get_height(sprCitymiddleground); // THEY ARE DIFFERENT SIZE, SO DIFF ORIGIN
    var xx = panelFinal.x + panelFinal.sprite_width/2;
    var yy = panelFinal.y + panelFinal.sprite_height/2;
    var d = -100;
    particle_create_ext(xx,
                        yy,
                        sprFire,0, 1, 0, c_white, 1, 8/60, 0,
                        0, 0, 0, c_white, 0, 0, d, 0, 0, 0);
    particle_create_ext(xx,
                        yy,
                        sprExplosion,0, 1.2, 0, c_white, 1, 3/60, 90,
                        0.035, 0.0005, 0, c_white, 0, -0.0001, d, 0, 0, 1);
    xx = view_xview[0];
    yy = view_yview[0];
    particle_create_ext(xx,
                        yy,
                        sprAnimeLines,0, 1.1, 0, c_white, 1, 13/60, 0,
                        0, 0, 0, c_white, 0, 0, d - 1, 0, 0, 0);
    particle_create_ext(xx,
                        yy,
                        sprBackgroundscape,0, 1.3, 0, c_white, 0.3, 10/60, 0,
                        0, 0, 0, c_white, 0, 0, 200, 0, 0, 0);
}



if state_timer>=(room_speed*2.5)
{
    state_switch("explode4");
}
