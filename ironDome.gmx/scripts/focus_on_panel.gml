/// focus_on_panel(obj, offset, lerp speed)
var view_w = default_view_w;
var view_h = default_view_h;
var focusPanel = argument0;
var w = focusPanel.sprite_width;
var h = focusPanel.sprite_height;
var xx = focusPanel.x + w/2;
var yy = focusPanel.y + h/2;
var fav_or = focusPanel.fav_orientation; // 0: noone, 1: horizontal, 2: vertical
var offset = 1 + argument1;
var lerpspd = argument2;
var final_view_w, final_view_h;

// if width larger, adjust based on width
if( ((w >= h) || (fav_or==1)) && (fav_or!=2) )
{
    final_view_w = w*offset;
    final_view_h = view_h * final_view_w / view_w;
}
// else adjust on height
else
{
    //show_debug_message("adjust on height")
    final_view_h = h*offset;
    final_view_w = view_w * final_view_h / view_h;
}

//show_debug_message("panel w: " + string(w) + ".final w: " + string(final_view_w));

// apply
x = xx; y = yy;
view_position_update_lerp(0,lerpspd,id);
view_size_update_lerp(0,lerpspd,final_view_w,final_view_h);    
view_angle_update_lerp(0,lerpspd,-image_angle);
