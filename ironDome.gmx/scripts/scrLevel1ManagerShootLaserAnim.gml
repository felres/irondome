var click = mouse_check_button_pressed(mb_left);
var panelplayerspawn = instance_find_team(objAnythingMask, "levelstart");
var focusChar = objLevel1FlyingDownChar;

if state_new 
{
    
    
    with instance_create(x, y, objLevel1IntroLaserDrawer)
        event_user(0);
        
    with focusChar
        sprite_index = sprPlayerLaserFire;
}

with instance_singleton(obj_camera)
{
    focus_on_panel(panelplayerspawn, other.panelfocusoffset, 0.2);
}

if objLevel1IntroLaserDrawer.ready
    state_switch("panCameraLaserSpawn");
