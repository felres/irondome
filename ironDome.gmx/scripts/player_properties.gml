/// Init variables

// basic info
collision_object = objObstacle;
max_image_speed = 11/60;

// input reading
leftInput = 0;
rightInput = 0;
upInput = 0;
downInput = 0;
actionInput = 0;
actionInputPressed = 0;
actionInputReleased = 0;
anyKey = 0;

/// movement
spd = 0;
spdMax = 8; //final spd
spdInit = 1; //initial speed
acc = 0.08;
decel = 0.19;
angleDir = 0;
turnspeed = 18;
// Direction of inputs
horDir = 0;
verDir = 0;
// movement speeds
horSpd = 0;
verSpd = 0;

/// Drawing
canMove = true;
canDrawSelf = true;
drawAng = 0;


/// FSM
state_machine_init();
state_create("idleDir", scrPlayerIdleDirectional);
state_create("moveDir", scrPlayerMoveDirectional);
state_create("idleHor", scrPlayerIdleHorizontal);
state_create("moveHor", scrPlayerMoveHorizontal);
state_create("broomSweep", scrPlayerBroomSweep);
state_create("broomLift", scrPlayerBroomLift);
state_init("broomSweep");
