/// draw_laser_blob(x1, y1, center r, outline r, segments, gauss variation, col1, col2)

var x1, y1, w, ow, segments, v, col1, col2;
x1 = argument0;
y1 = argument1;
w = argument2;
ow = argument3;
segments = argument4;
v = argument5;
col1 = argument6;
col2 = argument7;

for(var i = 0; i < segments; i++)
{
    var col = merge_colour(col1, col2, i/segments);
    draw_set_colour( col );
    var rad = lerp(w + ow, w,i/segments);
    
    draw_circle(x1, y1, gauss(rad, v), false);
}
draw_set_colour(col2);
draw_circle(x1, y1, gauss(w, v), false);


draw_set_color(c_white);
